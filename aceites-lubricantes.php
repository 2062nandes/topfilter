<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Aceites y lubricantes</h2>
      </div>
      <div class="col s12"><br></div>
    <div class="col l12">
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/sintetico.jpg" class="fotitem">
          <h3>Valvoline Sintéticos</h3>
          <ul>
            <li>- Soporta mucho mejor las  temperaturas frías.</li>
            <li>- Ayudan a  combatir el lodo y los depósitos generados por el residuo de la explosión del  combustible.</li>
            <li>- Aumenta la  vida útil del motor.</li>
            <li>- Es  superior en condiciones climáticas extremas ya sea cuando el clima es frío o  caliente.</li>
            <li>- Puede  aumentar la potencia o HP de su motor y puede economizar combustible.</li>
          </ul>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/semi-sintetico.jpg" class="fotitem">
          <h3>Valvoline Semisintéticos</h3>
          <ul>
            <li>- El aceite semisintético  posee un mayor índice de viscosidad.</li>
            <li>- Soporta temperaturas más bajas de fluidez. </li>
            <li>- Posee un punto de inflamación el lubricante semisintético. </li>
            <li>- Su resistencia a la oxidación es mayor. </li>
            <li>- Ahorro de aceite parcialmente sintético</li>
            <li>- El lubricante semisintético extiende  el cambio de lubricante.</li>
            <li>- Ahorra combustible y facilita el  arranque en frío </li>
            <li>- Posee una muy buena protección contra  el desgaste.</li>
          </ul>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/minterales.jpg" class="fotitem">
          <h3>Valvoline Minerales</h3>
          <ul>
            <li>- Brindar protección extra  frente al desgaste, generación de lodos/lacas y frente a la corrosión y la  herrumbre.</li>
            <li>- Protección a altas y bajas  temperaturas en desgastes del motor.</li>
            <li>- Combate la formación de depósitos de  lodo y barniz que reducen la vida del motor.</li>
            <li>- Reduce el consumo de aceite  combatiendo la volatilidad y la evaporación del aceite.</li>
            <li>- Resiste el espesamiento del aceite  brindando un control optimizado de la oxidación.</li>
          </ul>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/caja.jpg" class="fotitem">
          <h3>Valvoline de caja</h3>
          <ul>
            <li>- Los lubricantes para  caja manual tienen viscosidad adecuada.</li>
            <li>- Que el aceite para caja posee  protección contra la corrosión y la herrumbre.</li>
            <li>- Que el aceite de caja manual tenga la  capacidad de soportar presiones extremas.</li>
            <li>- Estabilidad térmica y estabilidad de  oxidación.</li>
            <li>- Contienen aditivos EP, anti  herrumbre, inhibidores de oxidación, agentes anti espumantes y desemulsionantes.</li>
          </ul>
        </div>
      </div>
    </div>
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/atf-4.jpg" class="fotitem">
          <h3>De transmisión automática<br>ATF + 4</h3>
          <ul>
            <li>- Soporta mucho mejor las  temperaturas frías.</li>
            <li>- Ayudan a  combatir el lodo y los depósitos generados por el residuo de la explosión del  combustible.</li>
            <li>- Aumenta la  vida útil del motor.</li>
            <li>- Es  superior en condiciones climáticas extremas ya sea cuando el clima es frío o  caliente.</li>
            <li>- Puede  aumentar la potencia o HP de su motor y puede economizar combustible.</li>
          </ul>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/cvt.jpg" class="fotitem">
          <h3>De transmisión automática<br>CVT</h3>
          <ul>
            <li>- El aceite semisintético  posee un mayor índice de viscosidad.</li>
            <li>- Soporta temperaturas más bajas de fluidez.</li>
            <li>- Posee un punto de inflamación el lubricante semisintético.</li>
            <li>- Su resistencia a la oxidación es mayor.</li>
            <li>- Ahorro de aceite parcialmente sintético</li>
            <li>- El lubricante semisintético extiende  el cambio de lubricante.</li>
            <li>- Ahorra combustible y facilita el  arranque en frío</li>
            <li>- Posee una muy buena protección contra  el desgaste.</li>
          </ul>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/max-life.jpg" class="fotitem">
          <h3>De transmisión automática<br>MAX LIFE</h3>
            <li>- Brindar protección extra  frente al desgaste, generación de lodos/lacas y frente a la corrosión y la  herrumbre.</li>
            <li>- Protección a altas y bajas  temperaturas en desgastes del motor.</li>
            <li>- Combate la formación de depósitos de  lodo y barniz que reducen la vida del motor.</li>
            <li>- Reduce el consumo de aceite  combatiendo la volatilidad y la evaporación del aceite.</li>
            <li>- Resiste el espesamiento del aceite  brindando un control optimizado de la oxidación.</li>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="item"><img data-original="imagenes/aceites/transmision-manual.jpg" class="fotitem">
          <h3>De transmisión de caja manual</h3>
          <ul>
            <li>- Los lubricantes para  caja manual tienen viscosidad adecuada.</li>
            <li>- Que el aceite para caja posee  protección contra la corrosión y la herrumbre.</li>
            <li>- Que el aceite de caja manual tenga la  capacidad de soportar presiones extremas.</li>
            <li>- Estabilidad térmica y estabilidad de  oxidación.</li>
            <li>- Contienen aditivos EP, anti  herrumbre, inhibidores de oxidación, agentes anti espumantes y desemulsionantes.</li>
          </ul>
          </p>
        </div>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
