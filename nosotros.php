<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Nuestra Empresa</h2>
        <p>Top Filter nació el 6 de octubre de 1997 en la ciudad de  Cochabamba, como una empresa unipersonal a la cabeza del Sr. William H. Román  Vásquez, representando, para el mercado departamental, a las marcas más  prestigiosas de filtros para motores: Mann y Luber Finer. A la fecha, esta  empresa se ha consolidado como líder en el mercado con un edificio propio y  tres tiendas ubicadas en la calle Santivañez esquina Tumusla.</p>
        <p> Actualmente, además de ofrecer productos de primera línea,  ha consolidado alianzas estratégicas con otros productos de alta calidad como  Sure de los Estados Unidos y Tec Fil del Brasil. Así mismo, la representación  del mejor aceite del país del norte, Valvoline, incluida su línea completa de  filtros para cajas automáticas de última generación.</p>
        <p> En el futuro, además de contribuir con cursos de  actualización para mecánicos, Top Filter tiene proyectado contar con un  lubricenter, fast service donde se brinde mantenimiento preventivo de primera  calidad. También tiene la intención de consolidar la fabricación de filtros y la  reconstrucción de filtros para maquinaria antigua</p>
      </div>
      <div class="col s12 m6 l6">
        <h2>Misión</h2>
        <p>Brindar al público usuario de Bolivia productos de primera  calidad para sus vehículos, satisfaciendo sus exigencias y necesidades de  filtros, lubricantes, aceites y aditivos con estándares del mercado  internacional que aplique nueva tecnología para ofrecer productos siempre de  mejor calidad que garanticen mayor eficiencia y durabilidad de sus motores.</p>
        <h2>Visión</h2>
        <p>Ser la empresa líder en Bolivia en la provisión de filtros,  lubricantes, aceites y aditivos para vehículos automotores como automóviles,  vehículos de carga pesada, transporte de pasajeros, maquinaria pesada y  agrícola, equipos mineros, utilitarios y motores industriales, estando siempre  a la vanguardia tecnológica del mercado y satisfaciendo los nuevos  requerimientos de nuestros clientes, para lograr el mejor desempeño y  competitividad de los productos que comercializamos. </p>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
