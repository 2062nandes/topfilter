<footer class="pie container">
  <div class="row" style="margin-left:0;margin-right:0;">
    <div class="col s12 m5 l3 colpie">
      <div class="col s12">
        <h1>Menu principal</h1>
        <ul>
          <li><a href="filtros-mann-filter.php"><i class="fa fa-caret-right"></i> Filtros por marcas</a></li>
            <li><a href="filtros-vehiculos-livianos.php"><i class="fa fa-caret-right"></i> Filtros para vehículos</a></li>
            <li><a href="aceites-lubricantes.php"><i class="fa fa-caret-right"></i> Aceites y lubricantes</a></li>
            <li><a href="aditivos.php"><i class="fa fa-caret-right"></i> Aditivos para autos</a></li>
        </ul>
      </div>
    </div>
    <div class="col s12 m7 l6 colpie">
      <div class="col s12">
        <h1>Dirección</h1>
        COCHABAMBA: calle Santivañez # 405 esq. Tumusla <a href="contactos.php">(VER MAPA)<br>
        </a>Telfs.: (591-2) 4252748 – 4584864<br>
        Fax: 4588591 <br>
        Email: topfilter78@hotmail.com
      </div>
    </div>
    <div class="col s12 m12 l3 colpie" style="border:none;">
      <div class="col s12 m6 l12">
        <h1>Derechos Reservados</h1>
          Top Filter
      </div>
      <div class="col s12 m6 l12">
        <h1>Diseño y programacion</h1>
        <a href="http://www.ahpublic.com" target="_blank">Ah! Publicidad</a>
      </div>
    </div>
  </div>
</footer>
  <!-- <script type="text/javascript" src="js/jquery.js"></script> -->
  <script type="text/javascript" src="js/jquery.lazyload.js"></script>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="vendor/materialize/materialize.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
