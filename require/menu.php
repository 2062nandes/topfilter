<header>
  <nav class="barra-inicio">
    <div class="nav-wrapper container">
      <ul class="right">
        <li><a href="./"><i class="fa fa-arrow-circle-right"></i> Inicio</a></li>
        <li><a href="nosotros.php"><i class="fa fa-arrow-circle-right"></i> Nosotros</a></li>
        <li><a href="contactos.php"><i class="fa fa-arrow-circle-right"></i> Contactos</a></li>
      </ul>
    </div>
  </nav>
  <nav class="contcabeza">
    <div class="nav-wrapper container">
        <a href="./" class="brand-logo">
<span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="0s"><span class="rojo">t</span></span><span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="0.4s"><span class="verde">o</span></span><span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="0.8s"><span class="azul">p</span> </span>
<span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="1.2s"><span class="amarillo">f</span></span><span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="1.6s"><span class="rojo">i</span></span><span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="2s"><span class="verde">l</span></span><span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="2.4s"><span class="azul">t</span></span><span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="2.8s"><span class="amarillo">e</span></span><span class="wow bounceInUp" data-wow-duration="2s" data-wow-delay="3.2s"><span class="rojo">r</span></span>
        <h2 class="slogan1 wow fadeInRight" data-wow-duration="2s" data-wow-delay="4s">ESPECIALISTAS EN FILTROS</h2>
        <h2 class="slogan2 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="4.2s">Las mejores marcas del mercado a su disposición</h2>
        </a>
    </div>
  </nav>
<nav class="menu-principal">
  <div class="nav-wrapper container">
    <div class="row menu-no-responsive" style="margin-left:0;margin-right:0;">
        <a class="col s12 m3 l3 dropdown-button waves-effect waves-light btn-large" data-beloworigin="true" data-activates="dropdown1" href="javascript:void(0)">
          <img src="imagenes/ico-marcas.png" width="40" height="40" align="absmiddle"> Filtros por marcas
        </a>
        <ul id="dropdown1" class="dropdown-content">
          <li><a class="center-align" href="filtros-mann-filter.php">Mann Filter</a></li><li class="divider"></li>
          <li><a class="center-align" href="filtros-luber-finer.php">Luber Finer</a></li><li class="divider"></li>
          <li><a class="center-align" href="filtros-fleet-guard.php">FleetGuard</a></li><li class="divider"></li>
          <li><a class="center-align" href="filtros-sure-filter.php">Sure Filter</a></li><li class="divider"></li>
          <li><a class="center-align" href="filtros-tec-fil.php">TecFil</a></li>
        </ul>
      <a class="col s12 m3 l3 dropdown-button waves-effect waves-light btn-large" data-beloworigin="true" data-activates="dropdown2" href="javascript:void(0)">
        <img src="imagenes/ico-filtros.png" width="40" height="40" align="absmiddle">  Filtros para vehículos</a>
      <ul id="dropdown2" class="dropdown-content">
          <li><a class="center-align" href="filtros-vehiculos-livianos.php">Vehículos livianos</a></li><li class="divider"></li>
          <li><a class="center-align" href="filtros-vehiculos-pesados.php">Vehículos pesados</a></li><li class="divider"></li>
          <li><a class="center-align" href="filtros-maquinaria-pesada.php">Maquinaria pesada</a></li><li class="divider"></li>
          <li><a class="center-align" href="filtros-maquinaria-industrial.php">Maquinaria industrial</a></li>
      </ul>
      <a href="aceites-lubricantes.php" class="col s12 m3 l3 waves-effect waves-light btn-large"><img src="imagenes/ico-lubricantes.png" width="40" height="40" align="absmiddle">  Aceites y lubricantes</a>
      <a href="aditivos.php" class="col s12 m3 l3 waves-effect waves-light btn-large"><img src="imagenes/ico-aditivos.png" width="40" height="40" align="absmiddle">  Aditivos para autos</a>
    </div>
    <!-- MENU ACORDION RESPONSIVE -->
    <div class="row menu-responsive" style="margin-left:0;margin-right:0;">
      <ul class="collapsible" data-collapsible="accordion">
        <li class="col s12 m6">
          <div class="collapsible-header btn-large"><img src="imagenes/ico-marcas.png" width="40" height="40" align="absmiddle"> Filtros por marcas</div>
          <div class="collapsible-body">
            <ul>
              <li class="col s12"><a class="center-align" href="filtros-mann-filter.php">Mann Filter</a></li><li class="divider"></li>
              <li class="col s12"><a class="center-align" href="filtros-luber-finer.php">Luber Finer</a></li><li class="divider"></li>
              <li class="col s12"><a class="center-align" href="filtros-fleet-guard.php">FleetGuard</a></li><li class="divider"></li>
              <li class="col s12"><a class="center-align" href="filtros-sure-filter.php">Sure Filter</a></li><li class="divider"></li>
              <li class="col s12"><a class="center-align" href="filtros-tec-fil.php">TecFil</a></li>
            </ul>
          </div>
        </li>
        <li class="col s12 m6">
          <div class="collapsible-header btn-large"><img src="imagenes/ico-filtros.png" width="40" height="40" align="absmiddle">  Filtros para vehículos</div>
          <div class="collapsible-body">
            <ul>
              <li class="col s12"><a class="center-align" href="filtros-vehiculos-livianos.php">Vehículos livianos</a></li><li class="divider"></li>
              <li class="col s12"><a class="center-align" href="filtros-vehiculos-pesados.php">Vehículos pesados</a></li><li class="divider"></li>
              <li class="col s12"><a class="center-align" href="filtros-maquinaria-pesada.php">Maquinaria pesada</a></li><li class="divider"></li>
              <li class="col s12"><a class="center-align" href="filtros-maquinaria-industrial.php">Maquinaria industrial</a></li>
            </ul>
          </div>
        </li>
        <div class="col s12">
        <li class="col s12 m6">
          <a href="aceites-lubricantes.php" class="col s12 waves-effect waves-light btn-large"><img src="imagenes/ico-lubricantes.png" width="40" height="40" align="absmiddle">  Aceites y lubricantes</a>
        </li>
        <li class="col s12 m6">
          <a href="aditivos.php" class="col s12 waves-effect waves-light btn-large"><img src="imagenes/ico-aditivos.png" width="40" height="40" align="absmiddle">  Aditivos para autos</a>
        </li>
        </div>
      </ul>
    </div>
  </div>
</nav>
</header>
