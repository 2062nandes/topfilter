<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Maquinaria pesada</h2>
      </div>
      <div class="col s12 m6 l6">
        <center>Solicite el FILTRO específico que necesite:<br>
          <a class="waves-effect waves-light btn btn-solicitar" href="reserva-filtros.php">SOLICITAR FILTRO</a>
        </center>
      </div>
      <div class="col s12"><br></div>
      <div class="boxlogos col s12 center-align">
         <img data-original="imagenes/log-pesados/case.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/jcb.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/john-deer.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/komatsu.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/massey-ferguson.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/new-holland.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/new-holland-construccion.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/valmet.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/valtra.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/atlas-copco.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/detroit.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/dynapac.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/cunumis.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/agrale.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/jcb.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/fiat.jpg" width="100" height="75" class="logos">
         <img data-original="imagenes/log-pesados/volvo.jpg" width="100" height="75" class="logos">
    	 </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
