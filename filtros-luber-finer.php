<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Luber Finer</h2>
      </div>
      <div class="col s12 m6 l6">
        <center>Solicite el FILTRO específico que necesite:<br>
          <a class="waves-effect waves-light btn btn-solicitar" href="reserva-filtros.php">SOLICITAR FILTRO</a>
        </center>
      </div>
      <div class="col s12"><br></div>
      <div class="col s12 m4 l3">
        <div class="item">
        <img data-original="imagenes/luber-finner/aire1.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/aire2.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/aire3.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/aire4.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/aceite1.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/aceite2.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/aceite3.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/aceite4.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/combustible1.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/combustible2.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/combustible3.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/combustible4.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/hidraulico1.jpg" class="fotitem">
        <h3>FILTRO<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/hidraulico2.jpg" class="fotitem">
        <h3>FILTRO<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/hidraulico3.jpg" class="fotitem">
        <h3>FILTRO<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/hidraulico4.jpg" class="fotitem">
        <h3>FILTRO<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/refrigerante1.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          CABINA</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/refrigerante2.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          REFRIGERANTE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/kit1.jpg" class="fotitem">
        <h3>AUTO TRANS FILTER KIT</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/luber-finner/kit2.jpg" class="fotitem">
        <h3>AUTO TRANS FILTER KIT</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
