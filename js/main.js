jQuery(function($) {'use strict',
	//Initiat WOW JS
	new WOW().init();
	$(document).ready(function(){
		$("img").lazyload({
			threshold : 150,
			effect : "fadeIn"
		});
    $('.collapsible').collapsible();
		$(".button-collapse").sideNav();
		$('select').material_select();
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
		$('.dropdown-button').dropdown({
	      inDuration: 300,
	      outDuration: 225,
	      constrainWidth: true, // Does not change width of dropdown to that of the activator
	      hover: true, // Activate on hover
	      gutter: 0, // Spacing from edge
	      belowOrigin: true, // Displays dropdown below the button
	      alignment: 'left', // Displays dropdown with edge aligned to the left of button
	      stopPropagation: false // Stops event propagation
	    }
	  );
  });
	/*Formulario de contacto*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });
  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn.contactenosform').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });
	$('.submitbtn.solicitud-filtro').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su solicitud fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su solicitud no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });
});
