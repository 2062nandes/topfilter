<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Sure Filter</h2>
      </div>
      <div class="col s12 m6 l6">
        <center>Solicite el FILTRO específico que necesite:<br>
          <a class="waves-effect waves-light btn btn-solicitar" href="reserva-filtros.php">SOLICITAR FILTRO</a>
        </center>
      </div>
      <div class="col s12"><br></div>
      <div class="col s12 m4 l3">
        <div class="item">
        <img data-original="imagenes/sure-filter/aceite1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/sure-filter/aceite2.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/sure-filter/aire1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/sure-filter/aire2.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/sure-filter/combustible1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/sure-filter/hidraulico1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/sure-filter/refrigerante1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/sure-filter/succion1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
