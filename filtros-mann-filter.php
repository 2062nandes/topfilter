<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Mann Filter</h2>
      </div>
      <div class="col s12 m6 l6">
        <center>Solicite el FILTRO específico que necesite:<br>
          <a class="waves-effect waves-light btn btn-solicitar" href="reserva-filtros.php">SOLICITAR FILTRO</a>
        </center>
      </div>
      <div class="col s12"><br></div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aire1.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aire2.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aire3.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aire4.jpg" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aceite1.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aceite2.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aceite3.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/aceite4.jpg" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/combustible1.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/combustible2.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/combustible3.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/combustible4.jpg" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/hifraulico1.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/hifraulico2.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/hifraulico3.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/hifraulico4.jpg" class="fotitem">
        <h3>FILTRO DE <br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/cabina1.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          CABINA</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/cabina2.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          CABINA</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/cabina3.jpg" class="fotitem">
        <h3>FILTRO DE<br>
          CABINA</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/man-filter/centrifuga1.jpg" class="fotitem">
        <h3>FILTRO DE <br>
          CENTRIFUGA</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
