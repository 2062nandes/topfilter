<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Vehículos pesados</h2>
      </div>
      <div class="col s12 m6 l6">
        <center>Solicite el FILTRO específico que necesite:<br>
          <a class="waves-effect waves-light btn btn-solicitar" href="reserva-filtros.php">SOLICITAR FILTRO</a>
        </center>
      </div>
      <div class="col s12"><br></div>
      <div class="boxlogos col s12 center-align">
          <img data-original="imagenes/vehiculos-pesados/volvo.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/scania.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/isuzu.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/hino.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/toyota.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/gmc.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/iveco.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/mercedes-benz.jpg" width="100" height="75" class="logos">
        	<img data-original="imagenes/vehiculos-pesados/ford.jpg" width="100" height="75" class="logos">
        	<img data-original="imagenes/vehiculos-pesados/dodge.jpg" width="100" height="75" class="logos">
        	<img data-original="imagenes/vehiculos-pesados/foton.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/nissan.jpg" width="100" height="75" class="logos">
        	<img data-original="imagenes/vehiculos-pesados/renault.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/wolkswagen.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/fiat.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/mack.jpg" width="100" height="75" class="logos"> 
          <img data-original="imagenes/vehiculos-pesados/freigh-liner.jpg" width="100" height="75" class="logos">
          <img data-original="imagenes/vehiculos-pesados/Kenworth.jpg" width="100" height="75" class="logos">  
    	 </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
