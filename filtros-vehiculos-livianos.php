<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Vehículos livianos</h2>
      </div>
      <div class="col s12 m6 l6">
        <center>Solicite el FILTRO específico que necesite:<br>
          <a class="waves-effect waves-light btn btn-solicitar" href="reserva-filtros.php">SOLICITAR FILTRO</a>
        </center>
      </div>
      <div class="col s12"><br></div>
      <div class="boxlogos col s12 center-align">
      	<img data-original="imagenes/logos/toyota.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/nissan.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/ford.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/mazda.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/misubishi.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/wolkswagen.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/suzuki.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/audi.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/bmw.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/volvo.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/honda.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/chevrolet.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/citroen.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/daihatsu.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/mercedes-benz.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/fiat.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/hyundai.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/jaguar.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/jeep.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/kia-motors.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/land-rover.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/peugeot.jpg" width="100" height="75" class="logos">
      	<img data-original="imagenes/logos/subaru.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/porsche.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/hummer.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/Scion.jpg" width="100" height="75" class="logos">
        <img data-original="imagenes/logos/lexus.jpg" width="100" height="75" class="logos">
    	 </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
