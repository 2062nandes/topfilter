<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <div class="container" style="margin-top: -20px;">
    <div id="wowslider-container1">
      <div class="ws_images">
        <ul>
          <li><img src="data1/images/slider-m1.jpg" srcset="data1/images/slider1.jpg 1280w, data1/images/slider-m1.jpg 1092w, data1/images/slider-s1.jpg 658w" alt="Filtros para vehículos" title="Filtros para vehículos" id="wows1_0"/></li>
          <li><img src="data1/images/slider-m2.jpg" srcset="data1/images/slider2.jpg 1280w, data1/images/slider-m2.jpg 1092w, data1/images/slider-s2.jpg 658w" alt="Aceites y lubricantes" title="Aceites y lubricantes" id="wows1_1"/></li>
          <li><img src="data1/images/slider-m3.jpg" srcset="data1/images/slider3.jpg 1280w, data1/images/slider-m3.jpg 1092w, data1/images/slider-s3.jpg 658w" alt="Aditivos" title="Aditivos" id="Aditivos"/></li>
        </ul>
      </div>
      <div class="ws_bullets">
        <div>
<!-- <a href="#" title="slider1"><img src="data1/tooltips/slider1.jpg" alt="slider1"/>1</a> <a href="#" title="slider2"><img src="data1/tooltips/slider2.jpg" alt="slider2"/>2</a> <a href="#" title="slider3"><img src="data1/tooltips/slider3.jpg" alt="slider3"/>3</a>  -->
</div>
      </div>
      <span class="wsl"><a href="http://wowslider.com">Slider Images</a> by WOWSlider.com v4.9</span>
      <div class="ws_shadow"></div>
    </div>
    <script type="text/javascript" src="engine1/wowslider.js"></script>
    <script type="text/javascript" src="engine1/script.js"></script>
  </div>
  <?php require('require/footer.php') ?>
  </body>
</html>
