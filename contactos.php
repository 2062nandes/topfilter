<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
    <section class="container">
      <div class="row">
        <div class="col s12 m4 l3">
          <h2>Contactos</h2>
          <form method="post" id="theForm" class="second" action="mail.php" role="form">
                    <div class="form_row">
                      <div class="input input-field">
                        <i class="material-icons prefix">account_circle</i>
                        <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
                        <label for="nombre">Nombre completo:</label>
                      </div>
                    </div>

                    <div class="form_row">
                      <div class="input input-field">
                        <i class="material-icons prefix">phone</i>
                        <input type="text" id="telefono" class="validate" name="telefono" tabindex="2" required>
                        <label for="telefono">Teléfono:</label>
                      </div>
                    </div>

                    <div class="form_row">
                      <div class="input input-field">
                        <i class="material-icons prefix">settings_cell</i>
                        <input type="text" id="movil" class="validate" name="movil" tabindex="3" required>
                        <label for="movil">Teléfono móvil:</label>
                      </div>
                    </div>

                    <div class="form_row">
                      <div class="input input-field">
                        <i class="material-icons prefix">location_on</i>
                        <input type="text" id="direccion" class="validate" name="direccion" tabindex="4" required>
                        <label for="direccion">Dirección:</label>
                      </div>
                    </div>

                    <div class="form_row">
                      <div class="input input-field">
                        <i class="material-icons prefix">location_city</i>
                        <input type="text" id="ciudad" class="validate" name="ciudad" tabindex="5" required>
                        <label for="ciudad">Ciudad:</label>
                      </div>
                    </div>
                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">email</i>
                          <label for="email">Su E-mail:</label>
                          <input type="email" id="email" class="validate" name="email" tabindex="6" required>
                        </div>
                      </div>

                      <div class="form_row mensaje">
                        <div class="input input-field">
                          <i class="material-icons prefix">mode_edit</i>
                          <label for="mensaje">Mensaje:</label>
                          <textarea id="mensaje" class="materialize-textarea validate" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
                        </div>
                      </div>
                      <div class="form_row botones center-align">
                        <i style="background-color: #0d47a1;" class="submitbtn contactenosform waves-effect waves-yellow btn z-depth-3 waves-input-wrapper" style=""><input class="waves-button-input" type="submit" tabindex="8" value="Enviar"></i>
                        <!-- <input class="submitbtn waves-effect waves-red" type="submit" tabindex="8" value="Enviar"> </input> -->
                        <!-- <input class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input> -->
                      </div>
                  <div class="col s12">
                    <div id="statusMessage"></div>
                  </div>
          </form>
        </div>
        <div class="col s12 m8 l9">
          <h2>Dirección</h2>
          <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.bo/maps/ms?msa=0&amp;msid=202692185879126865588.0004f133255d4475478ef&amp;ie=UTF8&amp;t=m&amp;ll=-17.393306,-66.161674&amp;spn=0.004095,0.010504&amp;z=17&amp;iwloc=0004f13327c6605dd6b7c&amp;output=embed"></iframe>
        </div>
      </div>
    </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
