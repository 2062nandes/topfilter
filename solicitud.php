<?php
    date_default_timezone_set("America/La_Paz");
    header('content-type: text/html; charset: utf-8');
    $hora=date("H:i:s");
    $fecha=date("d/m/Y");
    $ip=$_SERVER['REMOTE_ADDR'];

    $nombre = @trim(stripslashes($_POST['nombre']));
    $movil = @trim(stripslashes($_POST['movil']));
    $direccion = @trim(stripslashes($_POST['direccion']));
    $tipovehiculo = @trim(stripslashes($_POST['tipo-vehiculo']));
    $marca = @trim(stripslashes($_POST['marca']));
    $modelovehiculo = @trim(stripslashes($_POST['modelo-vehiculo']));
    $tipofiltro = @trim(stripslashes($_POST['tipo-filtro']));
    $combustible = @trim(stripslashes($_POST['combustible']));
    $comentario = @trim(stripslashes($_POST['comentario']));
    $email_to = @trim(stripslashes($_POST['area']));
    $email_to = "topfilter78@hotmail.com";
    $email_from = @trim(stripslashes($_POST['email']));
    $subject = "Solicitud de Filtro desde la web www.topfilterbolivia.com";
    $body = 'Nombre: '.$nombre."<br>";
    $body .= 'Teléfono móvil: '.$movil."<br>";
    $body .= 'Dirección: '.$direccion."<br>";
    $body .= 'Tipo de Vehículo: '.$tipovehiculo."<br>";
    $body .= 'Marca: '.$marca."<br>";
    $body .= 'Modelo del vehículo: '.$modelovehiculo."<br>";
    $body .= 'Tipo de Filtro: '.$tipofiltro."<br>";
    $body .= 'Combustible: '.$combustible."<br>";
    $body .= 'E-mail: '.$email_from."<br>";
    $body .= 'Comentario: '.$comentario."<br>";
    $body .= '---------------------'."<br>";
    $body .= 'Hora y fecha de envío: El '.$fecha.' a las '.$hora."<br>";
    $body .= 'IP del remitente: '.$ip;

    $headers = 'From: '.$email_from."\r\n";
    $headers .= 'MIME-Version: 1.0' ."\r\n";
    $headers .= 'Content-Type: text/HTML; charset=utf-8' ."\r\n";

    if( mail($email_to, $subject, $body, $headers) ){
        echo "enviado";
    }
    else{
        echo "nope";
    }
    // Return an appropriate response to the browser TRUE on sending, FALSE if not
?>
