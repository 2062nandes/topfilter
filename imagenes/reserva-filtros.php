<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12"><h2>Solicite el FILTRO específico que necesite</h2></div>
      <div class="col s12 m5 l4"><br>
        <form method="post" id="theForm" class="second" action="mail.php" role="form">
            <div class="form_row">
              <div class="input input-field">
                <i class="material-icons prefix">account_circle</i>
                <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
                <label for="nombre">Nombre completo:</label>
              </div>
            </div>

            <div class="form_row">
              <div class="input input-field">
                <i class="material-icons prefix">phone</i>
                <input type="text" id="telefono" class="validate" name="telefono" tabindex="2" required>
                <label for="telefono">Teléfono:</label>
              </div>
            </div>

            <div class="form_row">
              <div class="input input-field">
                <i class="material-icons prefix">settings_cell</i>
                <input type="text" id="movil" class="validate" name="movil" tabindex="3" required>
                <label for="movil">Teléfono móvil:</label>
              </div>
            </div>

            <div class="form_row">
              <div class="input input-field">
                <i class="material-icons prefix">location_on</i>
                <input type="text" id="direccion" class="validate" name="direccion" tabindex="4" required>
                <label for="direccion">Dirección:</label>
              </div>
            </div>

            <div class="form_row">
              <div class="input input-field">
                <i class="material-icons prefix">location_city</i>
                <input type="text" id="ciudad" class="validate" name="ciudad" tabindex="5" required>
                <label for="ciudad">Ciudad:</label>
              </div>
            </div>
              <div class="form_row">
                <div class="input input-field">
                  <i class="material-icons prefix">email</i>
                  <label for="email">Su E-mail:</label>
                  <input type="email" id="email" class="validate" name="email" tabindex="6" required>
                </div>
              </div>

              <div class="form_row mensaje">
                <div class="input input-field">
                  <i class="material-icons prefix">mode_edit</i>
                  <label for="mensaje">Mensaje:</label>
                  <textarea id="mensaje" class="materialize-textarea validate" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
                </div>
              </div>
              <div class="form_row botones center-align">
                <i style="background-color: #0d47a1;" class="submitbtn waves-effect waves-yellow btn z-depth-3 waves-input-wrapper" style=""><input class="waves-button-input" type="submit" tabindex="8" value="Enviar"></i>
                <!-- <input class="submitbtn waves-effect waves-red" type="submit" tabindex="8" value="Enviar"> </input> -->
                <!-- <input class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input> -->
              </div>
          <div class="col s12">
            <div id="statusMessage"></div>
          </div>
        </form>
      </div>
      <div class="col s12 m1 l2"></div>
      <div class="col s12 m6 l6">
        <h2>Marcas de filtros:</h2>
        <div class="textreserva center-align">
        	<img src="imagenes/manfilter.jpg" width="166" height="100" class="logfiltros">
        	<img src="imagenes/luber-finer.jpg" width="166" height="100" class="logfiltros">
          <img src="imagenes/fleetguard.jpg" width="166" height="100" class="logfiltros">
        	<img src="imagenes/sure-filter.jpg" width="166" height="100" class="logfiltros">
        	<img src="imagenes/tecfil.jpg" width="166" height="100" class="logfiltros">
        </div>
        <h2>Tipos de filtros:</h2>
        <ul>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
          <li> <i class="fa fa-caret-right"></i> </li>
        </ul>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
