<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Tratamiento de diesel</h2>
      </div>
      <div class="col s12"></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-diesel">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\limpiador-de-inyectores.png" alt="LIMPIADOR DE INYECTORES">
        </div>
        <div class="col s12 m8 l8">
          <h3>LIMPIADOR DE INYECTORES</h3>
          <ul>
            <li>•	Limpia los inyectores y todo el sistema  de alimentación y restablece la vaporización original de los inyectores.</li>
            <li>•	Mejora la calidad de ignición.</li>
            <li>•	Aumenta el índice de cetano y mejora la combustión del gasóleo.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18073 (W13966)</td>
                <td>5411693139666</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-diesel">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\tratamiento-diesel.png" alt="TRATAMIENTO DIESEL">
        </div>
        <div class="col s12 m8 l8">
          <h3>TRATAMIENTO DIESEL</h3>
          <ul>
            <li>•	Mejora el índice de cetano.</li>
            <li>•	Reduce los humos negros.</li>
            <li>•	Mejora el rendimiento energético del motor.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18043 (W51666)</td>
                <td>5411693516665</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
      <div class="col s12"></div>
<div class="col l6 m12 s12">
      <article class="col s12 offset-l3 aditivos-diesel">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\eliminador-de-humos.png" alt="ELIMINADOR DE HUMOS">
        </div>
        <div class="col s12 m8 l8">
          <h3>ELIMINADOR DE HUMOS</h3>
          <ul>
            <li>•	Mejora la combustión.</li>
            <li>•	Reduce la emisión de humos negros.</li>
            <li>•	Aumenta el rendimiento del motor y reduce el consumo.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18103 (W67966)</td>
                <td>5411693679667</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
      <div class="col s12"></div>
      <div class="col s12 m6 l6">
        <h2>Tratamiento de gasolina</h2>
      </div>
      <div class="col s12"><br></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-gasolina">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\limpiador-de-inyectores-gasolina.png" alt="LIMPIADOR DE INYECTORES GASOLINA">
        </div>
        <div class="col s12 m8 l8">
          <h3>LIMPIADOR DE INYECTORES GASOLINA</h3>
          <ul>
            <li>•	Suprime las suciedades tanto en los inyectores como en las válvulas.</li>
            <li>•	Evita los “tirones” en aceleración.</li>
            <li>•	Mejora el rendimiento energético del motor.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18063 (W55966)</td>
                <td>5411693559662</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-gasolina">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\tratamiento-de-gasolina.png" alt="TRATAMIENTO DE GASOLINA SUPER Y SIN PLOMO">
        </div>
        <div class="col s12 m8 l8">
          <h3>TRATAMIENTO DE GASOLINA SUPER Y SIN PLOMO</h3>
          <ul>
            <li>•	Lubrica las válvulas y la parte alta de los cilindros.</li>
            <li>•	Asegura una buena combustión y un rendimiento óptimo del motor.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18013 (W65266)</td>
                <td>5411693652660</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
      <div class="col s12"></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-gasolina">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\octane-booster.png" alt="AUMENTADOR DE POTENCIA OCTANE BOOSTER">
        </div>
        <div class="col s12 m8 l8">
          <h3>AUMENTADOR DE POTENCIA OCTANE BOOSTER</h3>
          <ul>
            <li>•	Aumenta el número de octano.</li>
            <li>•	Reduce el ruido del motor.</li>
            <li>•	Aumenta la potencia del motor y reduce el consumo.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18032 (W43872)</td>
                <td>5411693438721</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-gasolina">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\sustituto-del-plomo.png" alt="SUSTITUTO DEL PLOMO">
        </div>
        <div class="col s12 m8 l8">
          <h3>SUSTITUTO DEL PLOMO</h3>
          <ul>
            <li>•	Forma una capa protectora lubrificante en las válvulas y en los asientos de las mismas.</li>
            <li>•	Permite utilizar con plena garantía gasolina sin plomo en motores no preparados para este tipo de combustible.</li>
            <li>•	No ataca el catalizador.</li>
            <li>•	Fuel Starer para coches de gas.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18003 (W70611)</td>
                <td>5411693706110</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>250 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
<!--SEPARADOR DE SECCION-->
      <div class="col s12 m6 l6">
        <h2>Tratamiento de aceites</h2>
      </div>
      <div class="col s12"></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-aceites">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\tapa-fugas-aceite-de-motor.png" alt="TAPA FUGAS ACEITE DE MOTOR">
        </div>
        <div class="col s12 m8 l8">
          <h3>TAPA FUGAS ACEITE DE MOTOR</h3>
          <ul>
            <li>•	Previene y tapa las fugas de aceite de motor, sin desmontar.</li>
            <li>•	Lubrica y mantiene las juntas en buenas condiciones.</li>
            <li>•	Es compatible con todos los aceites minerales o sintéticos, gasolina o diesel.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18162 (W50660)</td>
                <td>5411693506604</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-aceites">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\mejorador-de-compresion.png" alt="MEJORADOR DE COMPRESIÓN">
        </div>
        <div class="col s12 m8 l8">
          <h3>MEJORADOR DE COMPRESIÓN</h3>
          <ul>
            <li>•	Reduce el consumo excesivo de aceite de motor.</li>
            <li>•	Restablece la compresión.</li>
            <li>•	Reduce los ruidos del motor y los humos de escape.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18133 (W51367)</td>
                <td>5411693513671</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
      <div class="col s12"></div>
      <div class="col s12 m6 l6">
        <h2>Refrigeración</h2>
      </div>
      <div class="col s12"></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-refrigeracion">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\tapa-fugas-radiador.png" alt="TAPA FUGAS RADIADOR">
        </div>
        <div class="col s12 m8 l8">
          <h3>TAPA FUGAS RADIADOR</h3>
          <ul>
            <li>•	Previene y detiene las fugas del radiador, bloque motor, calefacción, etc.</li>
            <li>•	Impide la formación de óxidos, la corrosión y las incrustaciones.</li>
            <li>•	Es miscible con todos los líquidos de refrigeración y anticongelantes.</li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18043 (W51666)</td>
                <td>5411693558665</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
<div class="col l6 m12 s12">
      <article class="col s12 aditivos-refrigeracion">
        <div class="col s12 m4 l4 center-align">
          <img data-original="imagenes\aditivos\radiator-flush.png" alt="LIMPIADOR RADIADOR/RADIATOR FLUSH">
        </div>
        <div class="col s12 m8 l8">
          <h3>LIMPIADOR RADIADOR/RADIATOR FLUSH</h3>
          <ul>
            <li>•	Elimina el óxido, las suciedades y las incrustaciones.</li>
            <li>•	Inofensivo para los manguitos, las juntas y los metales férricos o no férricos. </li>
          </ul>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="ref">Ref:</th>
                  <th data-field="ean">EAN</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>18182 (W56072)</td>
                <td>5411693560729</td>
              </tr>
            </tbody>
          </table>
          <table class="col s12 centered">
            <thead>
              <tr>
                  <th data-field="un-caja">Un/Caja</th>
                  <th data-field="capacidad">Capacidad</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>12</td>
                <td>325 ml.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </article></div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
