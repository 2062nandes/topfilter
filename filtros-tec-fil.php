<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12 m6 l6">
        <h2>Tecfil</h2>
      </div>
      <div class="col s12 m6 l6">
        <center>Solicite el FILTRO específico que necesite:<br>
          <a class="waves-effect waves-light btn btn-solicitar" href="reserva-filtros.php">SOLICITAR FILTRO</a>
        </center>
      </div>
      <div class="col s12"><br></div>
      <div class="col s12 m4 l3">
        <div class="item">
        <img data-original="imagenes/tec-fil/aire1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/aire2.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/aire3.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/aire4.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE AIRE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/aceite1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/aceite2.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/aceite3.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/aceite4.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE ACEITE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/combustible1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/combustible2.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/combustible3.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/combustible4.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE COMBUSTIBLE</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/hidraulico1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/hidraulico2.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO<br>
          HIDRAULICO</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/dehumidificador.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO<br>
          DEHUMIDIFICADOR</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/refrigeracion1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO DE REFRIGERACION<br></h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/separador1.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO SEPARADOR</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/separador2.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO SEPARADOR</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/separador3.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO SEPARADOR</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
      <div class="col s12 m4 l3">
        <div class="item"><img data-original="imagenes/tec-fil/separador4.jpg" width="200" height="200" class="fotitem">
        <h3>FILTRO SEPARADOR</h3>
        <div class="solicitar">
          <a href="reserva-filtros.php">Solicitar Filtro</a> </div>
        </div>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
