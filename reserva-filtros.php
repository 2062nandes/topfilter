<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php');?>
  <body>
  <?php require('require/menu.php');?>
  <section class="container">
    <div class="row">
      <div class="col s12"><h2>Solicite el FILTRO específico que necesite</h2></div>
      <div class="col s12 m5 l4"><br>
        <form method="post" id="theForm" class="second" action="solicitud.php" role="form">
            <div class="form_row">
              <div class="input input-field">
                <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
                <label for="nombre">Nombre completo:</label>
              </div>
            </div>
            <div class="form_row">
              <div class="input input-field">
                <input type="text" id="movil" class="validate" name="movil" tabindex="2" required>
                <label for="movil">Teléfono móvil:</label>
              </div>
            </div>
            <div class="form_row">
              <div class="input input-field">
                <input type="text" id="direccion" class="validate" name="direccion" tabindex="3" required>
                <label for="direccion">Dirección:</label>
              </div>
            </div>
            <div class="form_row">
              <div class="input input-field">
                <label for="email">Su E-mail:</label>
                <input type="email" id="email" class="validate" name="email" tabindex="4" required>
              </div>
            </div>
            <div class="form_row">
              <div class="input-field input">
                <label class="active">Tipo de Vehículo</label>
                <select class="form-control" id="tipo-vehiculo" type="text" name="tipo-vehiculo" tabindex="5" required>
                  <option value="" disabled selected>Seleccione su tipo de vehículo</option>
                  <option value="1">Automóvil</option>
                  <option value="2">Camioneta</option>
                  <option value="3">Bus</option>
                  <option value="4">Volqueta</option>
                  <option value="5">Motocicleta</option>
                  <option value="6">Remolque</option>
                </select>
              </div>
            </div>
            <div class="form_row">
              <div class="input input-field">
                <input type="text" id="marca" class="validate" name="marca" tabindex="6" required>
                <label for="marca">Marca: (ej: Toyota)</label>
              </div>
            </div>
            <div class="form_row">
              <div class="input input-field">
                <input type="text" id="modelo-vehiculo" class="validate" name="modelo-vehiculo" tabindex="7" required>
                <label for="modelo-vehiculo">Modelo de vehículo: (ej. Corolla)</label>
              </div>
            </div>
            <div class="form_row">
              <div class="input-field input">
                <label class="active">Tipo de Filtro</label>
                <select class="form-control" id="tipo-filtro" type="text" name="tipo-filtro" tabindex="8" required>
                  <option value="" disabled selected>Seleccione su tipo de filtro</option>
                  <option value="1">Filtro de aire</option>
                  <option value="2">Filtro de aceite</option>
                  <option value="3">Filtro de combustible</option>
                  <option value="4">Filtro de transmisión</option>
                  <option value="5">Filtro hidráulico</option>
                  <option value="6">Filtro de cabina</option>
                  <option value="7">Filtro de caja mecánica</option>
                  <option value="8">Filtro de caja automática</option>
                  <option value="9">Separadores de aceite</option>
                </select>
              </div>
            </div>
            <div class="form_row">
              <div class="input-field input">
                <label class="active">Combustible</label>
                <select class="form-control" id="combustible" type="text" name="combustible" tabindex="9" required>
                  <option value="" disabled selected>Seleccione su combustible</option>
                  <option value="1">Gasolina</option>
                  <option value="2">Diesel</option>
                  <option value="3">Gas Natural</option>
                </select>
              </div>
            </div>
            <div class="form_row mensaje">
              <div class="input input-field">
                <label for="comentario">Comentario:</label>
                <textarea id="comentario" class="materialize-textarea validate" cols="55" rows="7" name="comentario" tabindex="7"></textarea>
              </div>
            </div>
            <div class="form_row botones center-align">
              <i style="background-color: #0d47a1;" class="submitbtn solicitud-filtro waves-effect waves-yellow btn z-depth-3 waves-input-wrapper" style=""><input class="waves-button-input" type="submit" tabindex="8" value="Solicitar"></i>
              <!-- <input class="submitbtn waves-effect waves-red" type="submit" tabindex="8" value="Enviar"> </input> -->
              <!-- <input class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input> -->
            </div>
          <div class="col s12">
            <div id="statusMessage"></div>
          </div>
        </form>
      </div>
      <div class="col s12 m1 l2"></div>
      <div class="col s12 m6 l6 textreserva">
        <h2>Marcas de filtros:</h2>
        <div class="textreserva center-align">
        	<img data-original="imagenes/manfilter.jpg" width="166" height="100" class="logfiltros">
        	<img data-original="imagenes/luber-finer.jpg" width="166" height="100" class="logfiltros">
          <img data-original="imagenes/fleetguard.jpg" width="166" height="100" class="logfiltros">
        	<img data-original="imagenes/sure-filter.jpg" width="166" height="100" class="logfiltros">
        	<img data-original="imagenes/tecfil.jpg" width="166" height="100" class="logfiltros">
        </div>
        <h2>Tipos de filtros:</h2>
        <ul>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros de aire</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros de aceite</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros de combustible</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros de transmisión</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros hidráulicos</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros de cabina</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros de caja mecánica</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Filtros de caja automática</li>
          <li>&nbsp;&nbsp;  <i class="fa fa-caret-right"></i>&nbsp; Separadores de aceite</li>
        </ul>
      </div>
    </div>
  </section>
  <?php require('require/footer.php') ?>
  </body>
</html>
